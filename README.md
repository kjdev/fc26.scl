Fedora 26 - Software Collections Package Repository Repository
==============================================================

Repository Setup
----------------

Install with dnf repository.

```
% sudo tee -a /etc/yum.repos.d/kjdev.repo <<EOM
[kjdev-scl]
name=Fedora \$releasever - kjdev
baseurl=https://gitlab.com/kjdev/fc\$releasever.scl/raw/master/rpm/
enabled=0
gpgcheck=0

[kjdev-scl-source]
name=Fedora \$releasever - kjdev - Source
baseurl=https://gitlab.com/kjdev/fc\$releasever.scl/raw/master/source/
enabled=0
gpgcheck=0
EOM
```

RPM Install
-----------

```
% dnf install <PACKAGE>
% dnf --enablerepo kjdev-scl install <PACKAGE>
```

Use SCL
-------

```
% scl enable php70 'php -v'
PHP 7.0.22 (cli)
```

> Installed: dnf --enablerepo kjdev-scl install php70-php-cli
